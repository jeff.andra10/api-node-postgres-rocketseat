## api-node-postgres-rocketseat

Api para uma aplicação de nome Gobarber do curso da rocketseat.

## Banco de dados

- `Postgres`
- `MongoDB`
- `Redis`

## Postgres

DB_HOST=`127.0.0.1`
DB_USER=`docker`
DB_PASS=`docker`
DB_NAME=`gobarber`

## Mongo

MONGO_HOST=`localhost`
MONGO_PORT=`27017`
MONGO_NAME=`gobarber`

## Redis

REDIS_HOST=`0.0.0.0`
REDIS_PORT=`6379`

## Mail

MAIL_HOST=`smtp.mailtrap.io`
MAIL_PORT=`2525`
MAIL_USER=
MAIL_PASS=

## Sentry

SENTRY_DSN=

## API goBarber

This project was generated with [NodeJS] version 12.11.1.

## Development server

Run `yarn dev` for a dev server. Navigate to `http://localhost:3333/`. The app will automatically reload if you change any of the source files.

## Build

Run `yarn build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
