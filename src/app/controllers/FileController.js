import File from '../models/File';

/**
 * Controller para envio de arquivos de avatar
 * @class FileController
 */
class FileController {
  /**
   * @param {*} req
   * @param {*} res
   * @returns
   * @memberof FileController
   */
  async store(req, res) {
    const { originalname: name, filename: path } = req.file;

    const file = await File.create({
      name,
      path,
    });

    return res.json(file);
  }
}

export default new FileController();
