import User from '../models/User';
import File from '../models/File';

/**
 * Controller para seleção de servidores
 * @class ProviderController
 */
class ProviderController {
  /**
   * @param {*} req
   * @param {*} res
   * @returns
   * @memberof ProviderController
   */
  async index(req, res) {
    const providers = await User.findAll({
      where: { providers: true },
      attributes: ['id', 'name', 'avatar_id'],
      include: [
        {
          model: File,
          as: 'avatar',
          attributes: ['name', 'path'],
        },
      ],
    });

    return res.json(providers);
  }
}

export default new ProviderController();
