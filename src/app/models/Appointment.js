import { isBefore, subHours } from 'date-fns';

import Sequelize, { Model } from 'sequelize';

/**
 * @class Appointment
 * @extends {Model}
 */
class Appointment extends Model {
  /**
   * @static
   * @param {*} sequelize
   * @returns
   * @memberof Appointment
   */
  static init(sequelize) {
    super.init(
      {
        date: Sequelize.DATE,
        canceled_at: Sequelize.DATE,
        past: {
          type: Sequelize.VIRTUAL,
          get() {
            return isBefore(this.date, new Date());
          },
        },
        cancelable: {
          type: Sequelize.VIRTUAL,
          get() {
            return isBefore(new Date(), subHours(this.date, 2));
          },
        },
      },
      {
        sequelize,
      }
    );
    return this;
  }

  /**
   * @static
   * @param {*} models
   * @memberof Appointment
   */
  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
    this.belongsTo(models.User, { foreignKey: 'providers_is', as: 'provider' });
  }
}

export default Appointment;
