import Sequelize, { Model } from 'sequelize';

/**
 * @class File
 * @extends {Model}
 */
class File extends Model {
  /**
   * @static
   * @param {*} sequelize
   * @returns
   * @memberof File
   */
  static init(sequelize) {
    super.init(
      {
        path: Sequelize.STRING,
        name: Sequelize.STRING,
        url: {
          type: Sequelize.VIRTUAL,
          get() {
            return `${process.env.APP_URL}/files/${this.path}`;
          },
        },
      },
      {
        sequelize,
      }
    );

    return this;
  }
}

export default File;
