import bcrypt from 'bcryptjs';

import Sequelize, { Model } from 'sequelize';

/**
 * @class User
 * @extends {Model}
 */
class User extends Model {
  /**
   * @static
   * @param {*} sequelize
   * @returns
   * @memberof User
   */
  static init(sequelize) {
    super.init(
      {
        name: Sequelize.STRING,
        email: Sequelize.STRING,
        password: Sequelize.VIRTUAL,
        password_hash: Sequelize.STRING,
        provider: Sequelize.BOOLEAN,
      },
      {
        sequelize,
      }
    );

    this.addHook('beforeSave', async user => {
      if (user.password) {
        user.password_hash = await bcrypt.hash(user.password, 8);
      }
    });

    return this;
  }

  /**
   * @static
   * @param {*} models
   * @memberof User
   */
  static associate(models) {
    this.belongsTo(models.File, { foreignKey: 'avatar_id', as: 'avatar' });
  }

  /**
   * @param {*} password
   * @returns
   * @memberof User
   */
  checkPassword(password) {
    return bcrypt.compare(password, this.password_hash);
  }
}

export default User;
