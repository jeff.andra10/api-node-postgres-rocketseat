import Sequelize from 'sequelize';

import databaseConfig from '../config/database';
import Appointment from '../app/models/Appointment';
import File from '../app/models/File';
import User from '../app/models/User';

const mongoose = require('mongoose');

const models = [User, File, Appointment];

/**
 * @class Database
 */
class Database {
  constructor() {
    this.connection = new Sequelize(databaseConfig);
    this.init();
    this.associate();
    this.mongo();
  }

  /**
   * @memberof Database
   */
  init() {
    models.forEach(model => model.init(this.connection));
  }

  mongo() {
    this.connection = mongoose.connect(`${process.env.MONGODB_URI}/gobarber`, {
      useNewUrlParser: true,
      useFindAndModify: true,
    });
  }

  /**
   * @memberof Database
   */
  associate() {
    models.forEach(model => {
      if (model.associate) {
        model.associate(this.connection.models);
      }
    });
  }
}

export default new Database();
