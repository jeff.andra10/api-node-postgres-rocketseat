import * as Sentry from '@sentry/node';

import Bee from 'bee-queue';

import sentryConfig from '../config/sentry';
import CancellationMail from '../app/jobs/CancellationMail';

Sentry.init(sentryConfig);

const jobs = [CancellationMail];

/**
 * @class Queue
 */
class Queue {
  constructor() {
    this.queues = {};

    this.init();
  }

  init() {
    jobs.forEach(({ key, handle }) => {
      this.queues[key] = {
        bee: new Bee(key, {
          redis: {
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
          },
        }),
        handle,
      };
    });
  }

  /**
   * @param {*} queue
   * @param {*} job
   * @returns
   * @memberof Queue
   */
  add(queue, job) {
    return this.queues[queue].bee.createJob(job).save();
  }

  /**
   * @memberof Queue
   */
  processQueue() {
    jobs.forEach(job => {
      const { bee, handle } = this.queues[job.key];

      bee.on('failed', this.handleFailure).process(handle);
    });
  }

  /**
   * @param {*} job
   * @param {*} err
   * @memberof Queue
   */
  handleFailure(job, err) {
    if (process.env.NODE_ENV === 'development') {
      // eslint-disable-next-line no-console
      console.log(`Queue ${job.queue.name}: FAILED`, err);
    }

    Sentry.captureException(err);
  }
}

export default new Queue();
